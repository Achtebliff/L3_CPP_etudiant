using namespace std;
#include <iostream>
 
 int fiboRecursif(int x) {
		if (x <2) {
			return x;
		} else {
			return fiboRecursif(x-1)+fiboRecursif(x-2);
		}
	}	
	
	int fiboIteratif(int n) {
		int u = 0;
		int v = 1;
		int i, t;

		for(i = 2; i <= n; i++) {
			t = u + v;
			u = v;
			v = t;
		}
	return v;
	}
